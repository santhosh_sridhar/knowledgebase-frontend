import React from "react";
import { Redirect, Route } from "react-router";

export const PrivateRoute = ({ component: Component, ...rest }) => {
  const user = sessionStorage.getItem("user");
  return (
    <Route
      exact
      {...rest}
      render={(props) =>
        user ? <Component {...props} /> : <Redirect to="/signIn" />
      }
    />
  );
};
