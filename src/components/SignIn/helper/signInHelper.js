import axios from "axios";

export const signIn = (userDetails) => {
  const { username, password } = userDetails;
  return axios
    .post("/api/signIn", { username, password })
    .then((res) => {
      sessionStorage.setItem("user", res.data.user._id);
      return { hasError: false, message: res.data.message };
    })
    .catch((err) => {
      return { hasError: true, message: err.response.data.message };
    });
};
