import React, { useState } from "react";
import { Button, Form, FormControl, FormLabel, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import { signIn } from "../helper/signInHelper";
import { useHistory } from "react-router";
import { toast } from "react-toastify";

export const SignIn = (props) => {
  const history = useHistory();
  const [state, setState] = useState({
    username: "",
    password: "",
  });

  const handleChange = (e) => {
    setState({ ...state, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await signIn({
      username: state.username,
      password: state.password,
    });
    if (!response.hasError) {
      history.push("/");
    } else {
      toast.error(response.message);
    }
  };
  return (
    <Modal.Dialog centered>
      <Modal.Header>SignIn</Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Group>
            <FormLabel>Username</FormLabel>
            <FormControl
              type="text"
              name="username"
              value={state.username}
              onChange={handleChange}
              placeholder="Enter username"
              required
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              value={state.password}
              onChange={handleChange}
              placeholder="Password"
              required
            />
          </Form.Group>
          <Form.Group>
            <Form.Text>
              Don't have an account? <Link to="/signup">SignUp</Link>
            </Form.Text>
          </Form.Group>
          <Button className="mt-2" variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </Modal.Body>
    </Modal.Dialog>
  );
};
