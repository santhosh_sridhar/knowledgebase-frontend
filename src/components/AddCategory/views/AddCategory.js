import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { toast } from "react-toastify";
import { addCategory } from "../helper/addCategoryHelper";

export const AddCategory = (props) => {
  const { show, setShow, addNewCategory, userId } = props;
  const [name, setName] = useState("");
  const handleClick = () => {
    addCategory(userId, name).then((res) => {
      if (!res.hasError) {
        setName("");
        setShow(false);
        toast.success(res.message);
        addNewCategory(res.data);
      } else {
        toast.error(res.message);
      }
    });
  };
  return (
    <Modal show={show} onHide={() => setShow(false)} backdrop="static">
      <Modal.Header>
        <Modal.Title>Add Category</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Group>
          <Form.Label>Category</Form.Label>
          <Form.Control
            placeholder="Category Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => setShow(false)}>
          Close
        </Button>
        <Button onClick={handleClick}>Add</Button>
      </Modal.Footer>
    </Modal>
  );
};
