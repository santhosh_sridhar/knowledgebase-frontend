const axios = require("axios").default;

export const addCategory = (userId, categoryName) => {
  return axios
    .post("/api/category/addCategory", { name: categoryName, userId })
    .then((res) => {
      return {
        hasError: false,
        data: res.data.categories,
        message: res.data.message,
      };
    })
    .catch((err) => {
      return { hasError: true, message: "Failed to Add Category" };
    });
};
