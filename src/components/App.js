import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Jumbotron } from "react-bootstrap";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import { Category } from "./Category/Category";
import { PrivateRoute } from "./PrivateRoute";
import { SignIn } from "./SignIn/views/SignIn";
import { SignUp } from "./SignUp/views/SignUp";

function App() {
  return (
    <Router>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <Switch>
        <Route path="/signIn" component={SignIn} exact></Route>
        <Route path="/signUp" component={SignUp} exact></Route>
        <PrivateRoute path="/" exact component={Category}></PrivateRoute>
        <Route path="*">
          <Jumbotron fluid>
            <Container>
              <h1>404 Page Not Found</h1>
            </Container>
          </Jumbotron>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
