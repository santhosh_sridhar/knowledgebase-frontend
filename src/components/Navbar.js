import React from "react";
import { Button, Nav, Navbar } from "react-bootstrap";
import { useHistory } from "react-router";

export const NavBar = () => {
  const history = useHistory();

  const handleSignOut = () => {
    sessionStorage.removeItem("user");
    history.push("/signIn");
  };
  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="#home">Knowledge Base</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
        <Nav>
          {
            <Button
              className="ml-auto"
              variant="outline-info"
              onClick={handleSignOut}
            >
              SignOut
            </Button>
          }
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};
