import axios from "axios";

export const signUp = (userDetails) => {
  const { username, password } = userDetails;
  return axios
    .post("/api/signUp", { username, password })
    .then((res) => {
      return { hasError: false, message: res.data.message };
    })
    .catch((err) => {
      return { hasError: true, message: err.response.data };
    });
};
