import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { useHistory } from "react-router";
import { signUp } from "../helper/signupHelper";

export const SignUp = (props) => {
  const intitialValues = {
    username: "",
    password: "",
    checkPassword: "",
  };
  const history = useHistory();

  const [state, setState] = useState(intitialValues);
  const [error, setError] = useState(intitialValues);

  const handleChange = (e) => {
    setState({ ...state, [e.target.name]: e.target.value });
  };

  const validate = () => {
    const temp = {};
    if (state.password !== state.checkPassword)
      temp.password = "Passwords didn't match";

    setError(temp);

    return Object.values(temp).every((t) => t === "");
  };

  const handleSubmit = async (e) => {
    debugger;
    e.preventDefault();

    if (validate()) {
      const response = await signUp({
        username: state.username,
        password: state.password,
      });
      if (!response.hasError) {
        history.push("/signin");
      }
    }
  };
  return (
    <Modal.Dialog>
      <Modal.Header>SignUp</Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Group>
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Username"
              name="username"
              onChange={handleChange}
              value={state.username}
              required
            ></Form.Control>
            <Form.Text
              className="text-error"
              value={error.username}
            ></Form.Text>
          </Form.Group>
          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              placeholder="Password"
              onChange={handleChange}
              value={state.password}
              required
            ></Form.Control>
            <Form.Text
              className="text-error"
              value={error.password}
            ></Form.Text>
          </Form.Group>
          <Form.Group>
            <Form.Label>Re-type Password</Form.Label>
            <Form.Control
              type="password"
              name="checkPassword"
              placeholder="Re-type Password"
              onChange={handleChange}
              value={state.checkPassword}
              required
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Text>
              Already have an account? <a href="/signin">SignIn</a>
            </Form.Text>
          </Form.Group>
          <Button className="mt-3" type="submit">
            SignUp
          </Button>
        </Form>
      </Modal.Body>
    </Modal.Dialog>
  );
};
