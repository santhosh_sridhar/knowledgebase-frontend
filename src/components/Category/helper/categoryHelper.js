const axios = require("axios").default;

export const getCategories = (userId) => {
  return axios
    .get(`/api/category/getCategories/${userId}`)
    .then((res) => {
      return res.data.data;
    })
    .catch((err) => {});
};

// getCategories("60af227fc9f301315c1bfbce");
