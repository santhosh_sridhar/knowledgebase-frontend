import React, { useEffect, useState } from "react";
import { ListGroup, Tab, Row, Col, Button } from "react-bootstrap";
import { toast } from "react-toastify";
import { AddCategory } from "../AddCategory/views/AddCategory";
import { Content } from "../Content/Content";
import { updateContent } from "../Content/helper/contentHelper";
import { NavBar } from "../Navbar";
import { getCategories } from "./helper/categoryHelper";

export const Category = (props) => {
  const user = sessionStorage.getItem("user");
  const [show, setShow] = useState(false);
  const [categories, setCategories] = useState([]);
  const addCategory = (newCategories) => {
    setCategories([...newCategories]);
  };
  useEffect(() => {
    getCategories(user).then((res) => {
      setCategories([...res]);
    });
  }, [user]);
  const handleAddCategory = () => {
    setShow(true);
  };

  const handleUpdateContent = async (categoryId, content, file) => {
    const response = await updateContent(user, categoryId, content, file);
    if (!response.hasError) toast.success(response.message);
    else toast.error(response.message);
  };
  return (
    <div>
      <NavBar user={user} />
      <Tab.Container>
        <Row>
          <Col sm={3} className=" mt-3 ">
            <ListGroup>
              <ListGroup.Item>
                <h5 className="mr-5 d-inline">Category</h5>
                <Button
                  size="sm"
                  variant="light"
                  style={{
                    marginLeft: "10px",
                    // height: "20px",
                    fontWeight: "bolder",
                  }}
                  onClick={handleAddCategory}
                >
                  +
                </Button>
              </ListGroup.Item>
              {categories.map((category, i) => (
                <ListGroup.Item key={i} action href={`#${category.name}`}>
                  {category.name}
                </ListGroup.Item>
              ))}
            </ListGroup>
          </Col>
          <Col sm={8}>
            <Tab.Content>
              {categories.map((category, i) => {
                return (
                  <Tab.Pane key={i} eventKey={`#${category.name}`}>
                    <h1>{category.name} Content</h1>
                    <Content
                      categoryId={category._id}
                      content={category.content}
                      handleUpdateContent={handleUpdateContent}
                    />
                  </Tab.Pane>
                );
              })}
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
      <AddCategory
        show={show}
        setShow={setShow}
        userId={user}
        addNewCategory={addCategory}
      />
    </div>
  );
};
