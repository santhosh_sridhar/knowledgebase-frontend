import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";

export const Content = (props) => {
  const [content, setContent] = useState(props.content || "");
  const { categoryId, handleUpdateContent } = props;
  const [file, setFile] = useState({});
  const handleClick = () => {
    handleUpdateContent(categoryId, content, file);
  };
  return (
    <Form>
      <Form.Group>
        <Form.Control
          as="textarea"
          value={content}
          onChange={(e) => setContent(e.target.value)}
          rows={10}
        />
      </Form.Group>
      <Form.Group>
        <Form.File
          className="mt-2"
          multiple={false}
          onChange={(e) => setFile(e.target.files[0])}
        />
      </Form.Group>

      <Button onClick={handleClick} className="mt-3 text-center">
        Save
      </Button>
    </Form>
  );
};
