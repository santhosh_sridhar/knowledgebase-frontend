import axios from "axios";
import FormData from "form-data";

export const updateContent = (userId, categoryId, content, file) => {
  const form = new FormData();
  form.append("userId", userId);
  form.append("categoryId", categoryId);
  form.append("content", content);
  form.append("file", file);

  return axios
    .post("/api/category/updateContent", form, {
      headers: { "Content-Type": "multipart/form-data" },
    })
    .then((res) => {
      return {
        hasError: false,
        categories: res.data.categories,
        message: "Updated Content Successfully",
      };
    })
    .catch((err) => {
      return { hasError: true, message: "Failed to update Task" };
    });
};
